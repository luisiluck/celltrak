# Coding Challenge

## Requisites

* git
* JDK  1.8_X
* Maven

## Instructions

1. git clone https://bitbucket.org/luisiluck/celltrak.git
2. navigate to cloned project directory
3. run ``` mvn clean install```

