package com.celltrak.internal.qa_automation.exceptions;

public class InitException extends RuntimeException {
    public InitException(Throwable e) {
        super(e);
    }
}
