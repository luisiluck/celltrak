package com.celltrak.internal.qa_automation.api.model;

public enum Sort {
    DEFAULT(""),
    STARS("stars"),
    FORKS("forks"),
    UPDATED("updated");

    public String value;

    Sort(String value) {
        this.value = value;
    }
}
